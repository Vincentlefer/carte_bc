# Carte_BC

Script de base de création de cartes via QGIS et R

# Traitement des dossiers R
==> toujour mettre le code département en premire colone du fichier à ajouter / Attention de bien changer les Directory / Attention aux pakages nécéssaires 

-Import d'un fichier SHP

-Suppression des départements

-Import d'une table de données CSV

-Croisement des tables par le numéro de département

-Export sous format SHP via le pakage sf

# Traitement des fichiers dans Qgis
-Création d'un projet QGIS

-Lecture du fichier disponible dans la parti EXPORT // tuto: https://www.youtube.com/watch?v=Fg11ZDV2HYI

-Création des couleurs de fond sur une carte QGIS // tuto: https://www.youtube.com/watch?v=dZzGHR0jP0w (8:17 minute)

-Création des graphiques sur les communes // tuto: https://www.youtube.com/watch?v=PV4y-Rnuq6Q

